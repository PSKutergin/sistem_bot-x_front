import './styles/style.css';

import main from './modules/main';
import selects from './modules/selects'
import chat from './modules/chat'

main();
selects();
chat();